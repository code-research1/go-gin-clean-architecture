# Go Gin Clean Architecture

This project was created to learn golang with go gin framework

## How To Run

1. Run application with command `go run main.go`

## Feature

- [x] Database ORM
- [x] Database Relational
- [x] Json Validation
- [x] JWT Security
- [x] Open API / Swagger
- [x] Http Client
- [x] Error Handling
- [x] Logging
- [x] Repository Pattern
- [x] Encrypt & Decrypt
- [x] Uber FX
- [x] Xtra (Exmple Database & Json)
