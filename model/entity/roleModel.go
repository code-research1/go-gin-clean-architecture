package entity

import (
	"time"

	"github.com/go-gin-clean-architecture/utils"
)

type TableRoles struct {
	IdMasterRoles string    `json:"idMasterRoles"`
	RoleName      string    `json:"roleName"`
	Description   string    `json:"description"`
	CreatedBy     string    `json:"createdBy"`
	UpdatedBy     string    `json:"updatedBy"`
	IsActive      string    `json:"isActive"`
	CreatedAt     time.Time `json:"createdAt"`
	UpdatedAt     time.Time `json:"updatedAt"`
}

func (c TableRoles) TableName() string {
	return utils.NewEnv().DbSchema + ".master_roles"
}
