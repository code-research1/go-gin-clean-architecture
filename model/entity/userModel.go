package entity

import (
	"time"

	"github.com/go-gin-clean-architecture/utils"
)

type TableUser struct {
	IdMasterUsers   string     `json:"idMasterUsers"`
	IdMasterRoles   string     `json:"idMasterRoles"`
	Fullname        string     `json:"fullname"`
	Username        string     `json:"username"`
	IsGender        string     `json:"isGender"`
	Address         string     `json:"address"`
	HpNumber        string     `json:"hpNumber"`
	DateActivation  time.Time  `json:"dateActivation"`
	Email           string     `json:"email"`
	EmailVerifiedAt time.Time  `json:"emailVerifiedAt"`
	Password        string     `json:"-"`
	UrlPhoto        string     `json:"urlPhoto"`
	CreatedBy       string     `json:"createdBy"`
	UpdatedBy       string     `json:"updatedBy"`
	IsActive        string     `json:"isActive"`
	CreatedAt       time.Time  `json:"createdAt"`
	UpdatedAt       time.Time  `json:"updatedAt"`
	Role            TableRoles `json:"role,omitempty" gorm:"foreignKey:IdMasterRoles;references:IdMasterRoles"`
}

func (u TableUser) TableName() string {
	return utils.NewEnv().DbSchema + ".master_users"
}
