package criteria

type UserSearchCriteria struct {
	Key   string `json:"key" binding:"required"`
	Value string `json:"value" binding:"required"`
}

type StoreUserCriteria struct {
	IdMasterRoles string `json:"idMasterRoles" validate:"required"`
	Fullname      string `json:"fullname" validate:"required"`
	Username      string `json:"username" validate:"required"`
	IsGender      string `json:"isGender" validate:"required"`
	Address       string `json:"address" validate:"required"`
	HpNumber      string `json:"hpNumber" validate:"required"`
	Email         string `json:"email" validate:"required"`
	CreatedBy     string `json:"createdBy" validate:"required"`
}

type UpdateUserCriteria struct {
	IdMasterRoles string `json:"idMasterRoles" validate:"required"`
	Fullname      string `json:"fullname" validate:"required"`
	Username      string `json:"username" validate:"required"`
	IsGender      string `json:"isGender" validate:"required"`
	Address       string `json:"address" validate:"required"`
	HpNumber      string `json:"hpNumber" validate:"required"`
	Email         string `json:"email" validate:"required"`
	UpdatedBy     string `json:"updatedBy" validate:"required"`
}

type UpdateIsActiveUserCriteria struct {
	IsActive  string `json:"isActive" validate:"required"`
	UpdatedBy string `json:"updatedBy" validate:"required"`
}

type GetListOfUserOptions struct {
	SearchBy string   `json:"search_by"`
	Status   []string `json:"status"`
}
