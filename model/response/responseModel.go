package response

type DataPagingResult struct {
	PageNumber       int         `json:"pageNumber"`
	PageSize         int         `json:"pageSize"`
	TotalRecordCount int64       `json:"totalRecordCount"`
	Records          interface{} `json:"records"`
}

type ResponseData struct {
	ResponseCode        int         `json:"responseCode"`
	ResponseDescription string      `json:"responseDescription"`
	ResponseTime        string      `json:"responseTime"`
	ResponseDatas       interface{} `json:"responseDatas"`
}

type SetHeaderParams struct {
	Authorization string `json:"Authorization"`
	Accept        string `json:"Accept"`
	ContentType   string `json:"Content-Type"`
	ApiKey        string `json:"Api-Key"`
	Signature     string `json:"Signature"`
	SignatureTime int    `json:"Signature-Time"`
}
