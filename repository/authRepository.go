package repository

import (
	"errors"
	"fmt"

	"github.com/go-gin-clean-architecture/model/entity"
	"github.com/go-gin-clean-architecture/utils"
	"gorm.io/gorm"
)

type AuthRepository struct {
	utils.Database
	logger      utils.Logger
	commonUtils utils.CommonUtils
}

func NewAuthRepository(db utils.Database,
	logger utils.Logger,
	commonUtils utils.CommonUtils,
) AuthRepository {
	return AuthRepository{
		Database:    db,
		logger:      logger,
		commonUtils: commonUtils,
	}
}

func (r AuthRepository) CheckLogin(username string, password string) (entity.TableUser, error) {

	var objUser entity.TableUser

	if err := r.Select("*").Where("username = ?", username).First(&objUser).Error; err != nil {
		errors.Is(err, gorm.ErrRecordNotFound)
		return objUser, err
	}

	match, err := r.commonUtils.VerifyPassword(password, objUser.Password)
	if !match {
		fmt.Println("Hash and password doesn't match.")
		return objUser, err
	}

	return objUser, err

}
