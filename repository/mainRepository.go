package repository

import "go.uber.org/fx"

var Module = fx.Options(
	fx.Provide(NewAuthRepository),
	fx.Provide(NewRoleRepository),
	fx.Provide(NewUserRepository),
)
