package repository

import (
	"context"
	"errors"
	"strings"

	"github.com/go-gin-clean-architecture/model/criteria"
	"github.com/go-gin-clean-architecture/model/entity"
	"github.com/go-gin-clean-architecture/utils"
	"github.com/go-gin-clean-architecture/utils/paginate"
	"gorm.io/gorm"
)

type UserRepository struct {
	utils.Database
	logger utils.Logger
}

func NewUserRepository(db utils.Database, logger utils.Logger) UserRepository {
	return UserRepository{
		Database: db,
		logger:   logger,
	}
}

func (r UserRepository) GetAllDataUser(ctx context.Context, paging paginate.Datapaging, options criteria.GetListOfUserOptions) (int64, *[]entity.TableUser, error) {

	var userDatas []entity.TableUser

	query := r.WithContext(ctx).Table("db_staterkit.master_users").Preload("Role")

	if len(options.Status) > 0 {
		statusValuesStr, err := paginate.PrepareStatusValues(options.Status)
		if err != nil {
			return 0, nil, err
		}

		query.Where("is_active IN ? ", statusValuesStr)
	}

	if len(paging.FilterValue) > 0 && len(options.SearchBy) > 0 {
		if options.SearchBy == "fullname" {
			query.Where("upper(fullname) like  ? ", "%"+strings.ToUpper(paging.FilterValue)+"%")
		}

		if options.SearchBy == "username" {
			query.Where("upper(username) like  ? ", "%"+strings.ToUpper(paging.FilterValue)+"%")
		}

		if options.SearchBy == "address" {
			query.Where("upper(address) like  ? ", "%"+strings.ToUpper(paging.FilterValue)+"%")
		}
	}

	var totalCount int64
	query.Model(&userDatas).Count(&totalCount)
	if query.Error != nil {
		return 0, nil, query.Error
	}

	paging.BuildQueryGORM(query).Find(&userDatas)

	return totalCount, &userDatas, query.Error

}

func (r UserRepository) GetAllDataUserByParam(ctx context.Context, key string, value string) ([]entity.TableUser, error) {

	var userDatas []entity.TableUser

	if err := r.DB.WithContext(ctx).Preload("Role").Where(key, value).Find(&userDatas).Error; err != nil {
		errors.Is(err, gorm.ErrRecordNotFound)
		return userDatas, err
	}

	return userDatas, nil

}

func (r UserRepository) GetDataUserById(ctx context.Context, idMasterUsers string) (entity.TableUser, error) {

	var userDatas entity.TableUser

	if err := r.WithContext(ctx).Preload("Role").Unscoped().Where("id_master_users = ?", idMasterUsers).First(&userDatas).Error; err != nil {

		errors.Is(err, gorm.ErrRecordNotFound)
		return userDatas, err
	}
	return userDatas, nil

}

func (r UserRepository) Store(ctx context.Context, userDatas entity.TableUser) (entity.TableUser, error) {

	if err := r.Create(&userDatas).Error; err != nil {

		errors.Is(err, gorm.ErrRecordNotFound)
		return userDatas, err
	}
	return userDatas, nil
}

func (r UserRepository) Update(ctx context.Context, userDatas entity.TableUser) (entity.TableUser, error) {

	if err := r.WithContext(ctx).Where("id_master_users = ?", userDatas.IdMasterUsers).Updates(&userDatas).Error; err != nil {
		errors.Is(err, gorm.ErrRecordNotFound)
		return userDatas, err
	}
	return userDatas, nil
}
