package repository

import (
	"context"
	"errors"
	"strings"

	"github.com/go-gin-clean-architecture/model/criteria"
	"github.com/go-gin-clean-architecture/model/entity"
	"github.com/go-gin-clean-architecture/utils"
	"github.com/go-gin-clean-architecture/utils/paginate"
	"gorm.io/gorm"
)

type RoleRepository struct {
	utils.Database
	logger utils.Logger
}

func NewRoleRepository(db utils.Database, logger utils.Logger) RoleRepository {
	return RoleRepository{
		Database: db,
		logger:   logger,
	}
}

func (r RoleRepository) GetAllDataRole(ctx context.Context, paging paginate.Datapaging, options criteria.GetListOfRoleOptions) (int64, *[]entity.TableRoles, error) {

	var roleDatas []entity.TableRoles

	query := r.WithContext(ctx).Table("db_staterkit.master_roles")

	if len(options.Status) > 0 {
		statusValuesStr, err := paginate.PrepareStatusValues(options.Status)
		if err != nil {
			return 0, nil, err
		}

		query.Where("is_active IN ? ", statusValuesStr)
	}

	if len(paging.FilterValue) > 0 && len(options.SearchBy) > 0 {
		if options.SearchBy == "role_name" {
			query.Where("upper(role_name) like  ? ", "%"+strings.ToUpper(paging.FilterValue)+"%")
		}
	}

	var totalCount int64
	query.Model(&roleDatas).Count(&totalCount)
	if query.Error != nil {
		return 0, nil, query.Error
	}

	paging.BuildQueryGORM(query).Find(&roleDatas)

	return totalCount, &roleDatas, query.Error

}

func (r RoleRepository) GetDataRoleById(ctx context.Context, idMasterRoles string) (entity.TableRoles, error) {

	var roleDatas entity.TableRoles

	if err := r.WithContext(ctx).Unscoped().Where("id_master_roles = ?", idMasterRoles).First(&roleDatas).Error; err != nil {

		errors.Is(err, gorm.ErrRecordNotFound)
		return roleDatas, err
	}
	return roleDatas, nil

}

func (r RoleRepository) Store(ctx context.Context, roleDatas entity.TableRoles) (entity.TableRoles, error) {

	if err := r.Create(&roleDatas).Error; err != nil {

		errors.Is(err, gorm.ErrRecordNotFound)
		return roleDatas, err
	}
	return roleDatas, nil
}

func (r RoleRepository) Update(ctx context.Context, roleDatas entity.TableRoles) (entity.TableRoles, error) {

	if err := r.WithContext(ctx).Where("id_master_roles = ?", roleDatas.IdMasterRoles).Updates(&roleDatas).Error; err != nil {
		errors.Is(err, gorm.ErrRecordNotFound)
		return roleDatas, err
	}

	return roleDatas, nil
}
