package validation

import (
	"encoding/json"
	"log"

	"github.com/go-playground/validator/v10"
)

func ValidateCriteria(modelValidate interface{}) error {
	validate := validator.New()

	if err := validate.Struct(modelValidate); err != nil {
		var messages []map[string]interface{}
		for _, err := range err.(validator.ValidationErrors) {
			messages = append(messages, map[string]interface{}{
				"field":   err.Field(),
				"message": "this field is " + err.Tag(),
			})
		}

		jsonMessage, errJson := json.Marshal(messages)
		if errJson != nil {
			log.Printf("JSON marshaling error: %v\n", errJson)
			return errJson
		}

		// Handle the error here and return it as needed
		return CustomError{Message: string(jsonMessage)}
	}

	return nil
}

func (e CustomError) Error() string {
	return e.Message
}

type CustomError struct {
	Message string
}
