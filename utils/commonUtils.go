package utils

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"reflect"
	"runtime"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-gin-clean-architecture/constants"
	"github.com/go-gin-clean-architecture/model/response"
	"github.com/google/uuid"
	"github.com/mergermarket/go-pkcs7"
	"golang.org/x/crypto/bcrypt"
	"golang.org/x/crypto/pbkdf2"
)

type CommonUtils struct {
	handler  RequestHandler
	logger   Logger
	config   Config
	database Database
}

func NewCommonUtils(
	handler RequestHandler,
	logger Logger,
	config Config,
	database Database,
) CommonUtils {
	return CommonUtils{
		handler:  handler,
		logger:   logger,
		config:   config,
		database: database,
	}
}

func (u CommonUtils) DateToStdNow() string {
	currentTime := time.Now()
	return currentTime.Format("2006-01-02 15:04:05")
}

func (u CommonUtils) GetFunctionName(i interface{}) string {
	return runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()
}

func (u CommonUtils) GetLineNumber() int {
	_, fn, line, _ := runtime.Caller(1)
	fmt.Printf("DEBUG   | %s:%d | %v\n", fn, line, "e")

	return line
}

func (u CommonUtils) HashPassword(password string) (interface{}, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return bytes, err
}

func (u CommonUtils) VerifyPassword(password, hash string) (bool, error) {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	if err != nil {
		return false, err
	}

	return true, nil
}

func (u CommonUtils) HmacEncode(msg, key []byte) string {
	mac := hmac.New(sha256.New, key)
	mac.Write(msg)

	return hex.EncodeToString(mac.Sum(nil))
}

func (u CommonUtils) HmacDecode(msg, key []byte, hash string) (bool, error) {
	sig, err := hex.DecodeString(hash)
	if err != nil {
		return false, err
	}

	mac := hmac.New(sha256.New, key)
	mac.Write(msg)

	return hmac.Equal(sig, mac.Sum(nil)), nil
}

func (u CommonUtils) ResponBody(status int,
	description string,
	datas any) response.ResponseData {

	var result response.ResponseData

	result.ResponseCode = status
	result.ResponseDescription = description
	result.ResponseTime = u.DateToStdNow()
	result.ResponseDatas = datas

	return result
}

func (u CommonUtils) GetJSONRawBody(c *gin.Context) map[string]string {

	jsonBody := make(map[string]string)
	err := json.NewDecoder(c.Request.Body).Decode(&jsonBody)
	if err != nil {

		// log.Error("empty json body")
		return nil
	}

	return jsonBody
}

func (u CommonUtils) StringToInteger(parameter string) int {

	n, _ := strconv.Atoi(parameter)

	return n
}

func (u CommonUtils) CreateLog(logFlagging, title, message string, code int, endpoint, linenumber, datas interface{}) {

	if logFlagging == constants.ERROR {

		globalLogger.Errorw(message,
			constants.TITLE, title,
			constants.CODE, code,
			constants.ENDPOINT, endpoint,
			constants.LINENUMBER, linenumber,
			constants.DATA, datas,
		)

	} else if logFlagging == constants.WARNING {

		globalLogger.Warnw(message,
			constants.CODE, code,
			constants.ENDPOINT, endpoint,
			constants.LINENUMBER, linenumber,
			constants.DATA, datas,
		)

	} else if logFlagging == constants.INFORMATION {

		globalLogger.Infow(message,
			constants.CODE, code,
			constants.ENDPOINT, endpoint,
			constants.LINENUMBER, linenumber,
			constants.DATA, datas,
		)

	}
}

func (u CommonUtils) GeneratePassword() (string, error) {
	password := "p@ssw0rd"

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return string(hashedPassword), nil
}

// / Decrypt - Encrypt with current condition
func (u CommonUtils) EncryptAes256(dataPayload string, password string, iv string) (string, error) {
	block, errChipper := aes.NewCipher([]byte(password))

	if errChipper != nil {
		return "", fmt.Errorf("decode input text is error. error: " + errChipper.Error())
	}

	if dataPayload == "" {
		return "", fmt.Errorf("data payload error")
	}

	ecb := cipher.NewCBCEncrypter(block, []byte(iv))

	content := []byte(dataPayload)
	content, errPad := pkcs7.Pad(content, block.BlockSize())
	if errPad != nil {
		return "", fmt.Errorf("decode input text is error. error: " + errPad.Error())
	}

	crypted := make([]byte, len(content))
	ecb.CryptBlocks(crypted, content)

	return base64.StdEncoding.EncodeToString(crypted), nil

}

func (u CommonUtils) DecryptAes256(dataPayload string, password string, iv string) ([]byte, error) {

	ciphertext, errDecode := base64.StdEncoding.DecodeString(dataPayload)
	if errDecode != nil {
		return nil, fmt.Errorf("decode input text is error. error: " + errDecode.Error())
	}

	block, errChipper := aes.NewCipher([]byte(password))

	if errChipper != nil {
		return nil, fmt.Errorf("key chipper is error : " + errChipper.Error())
	}

	if len(ciphertext)%aes.BlockSize != 0 {
		return nil, fmt.Errorf("ciphertext is not a multiple of the block size")
	}

	blockMode := cipher.NewCBCDecrypter(block, []byte(iv))
	blockMode.CryptBlocks(ciphertext, ciphertext)

	cipherUnPad, _ := pkcs7.Unpad(ciphertext, aes.BlockSize)

	return cipherUnPad, nil
}

// / Encrypt-Decrypt with current condition
func deriveKey(password, salt []byte, iterations, keySize int) []byte {
	return pbkdf2.Key(password, salt, iterations, keySize, sha256.New)
}

func EncryptAes256Sha256(dataPayload string, password string) (string, error) {
	// Create salt and hash from password
	uuid := uuid.New()
	salt := uuid[:]

	// Create IV from random 16 byte
	iv := make([]byte, 16)
	rand.Read(iv)
	byteIv := []byte(iv)

	// Create password from hash, salt, iteration, and how many bytes
	key := deriveKey([]byte(password), salt, 5, 32)
	block, errChipper := aes.NewCipher(key)

	if errChipper != nil {
		fmt.Println("key chipper is error :")
	}

	if dataPayload == "" {
		fmt.Println("input plain text is empty")
	}

	cbc := cipher.NewCBCEncrypter(block, byteIv)

	content := []byte(dataPayload)
	content, errPad := pkcs7.Pad(content, block.BlockSize())
	if errPad != nil {
		fmt.Println("padding input text padding is error")
	}

	crypted := make([]byte, len(content))
	cbc.CryptBlocks(crypted, content)

	crypted = append(append(salt, byteIv...), crypted...)
	return base64.StdEncoding.EncodeToString(crypted), nil
}

func DecryptAes256Sha256(dataPayload string, password string) (string, error) {
	// Get Hash from
	ciphertext, errDecode := base64.StdEncoding.DecodeString(dataPayload)
	if errDecode != nil {
		return "", fmt.Errorf("decode input text is error. error: " + errDecode.Error())
	}
	// Create salt and iv from encrypted text
	salt := ciphertext[:16]
	iv := ciphertext[16:32]
	ciphertext = ciphertext[32:]

	// Create key from hash, salt, iteration, and how many bytes
	key := deriveKey([]byte(password), salt, 5, 32)
	block, errChipper := aes.NewCipher(key)

	if errChipper != nil {
		return "", fmt.Errorf("key chipper is error : " + errChipper.Error())
	}

	if len(ciphertext)%aes.BlockSize != 0 {
		return "", fmt.Errorf("ciphertext is not a multiple of the block size")
	}

	blockMode := cipher.NewCBCDecrypter(block, iv)
	blockMode.CryptBlocks(ciphertext, ciphertext)

	cipherUnPad, err := pkcs7.Unpad(ciphertext, aes.BlockSize)
	if err != nil {
		return "", err
	}

	return string(cipherUnPad), nil
}

func (u CommonUtils) DetectJSONOrString(s string) (interface{}, bool) {
	var js map[string]interface{}

	if json.Unmarshal([]byte(s), &js) == nil {
		return js, true // It's valid JSON
	}

	return s, false // It's not valid JSON, treat as a plain string
}

func (u CommonUtils) ConvertToBytes(value interface{}) ([]byte, error) {
	// Use reflection to get the type of the value.
	valueType := reflect.TypeOf(value)

	// Use a switch statement to handle different types.
	switch valueType.Kind() {
	case reflect.String:
		return []byte(value.(string)), nil
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return []byte(fmt.Sprintf("%d", value)), nil
	case reflect.Float32, reflect.Float64:
		return []byte(fmt.Sprintf("%f", value)), nil
	case reflect.Bool:
		return []byte(fmt.Sprintf("%t", value)), nil
	default:
		return nil, fmt.Errorf("Unsupported type: %v", valueType)
	}
}

/// End of Encrypt-Decrypt with new condition
