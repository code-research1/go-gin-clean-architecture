package utils

import (
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"go.elastic.co/ecszap"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

// Declare
type Logger struct {
	Logger *zap.SugaredLogger
}

var (
	globalLogger *zap.SugaredLogger
)

func NewLogger(config Config) Logger {
	if gin.IsDebugging() {
		// Zapcore sync lumberjack
		logWriter := zapcore.AddSync(&lumberjack.Logger{
			Filename:   config.LogDirectory,
			MaxSize:    500, // megabytes
			MaxBackups: 3,
			MaxAge:     1,    //days
			Compress:   true, // disabled by default
		})
		encoderConfig := ecszap.ECSCompatibleEncoderConfig(zap.NewDevelopmentEncoderConfig())
		encoderConfig.EncodeTime = zapcore.TimeEncoderOfLayout(time.RFC3339)
		encoder := zapcore.NewJSONEncoder(encoderConfig)
		consoleEncoderConfig := ecszap.NewDefaultEncoderConfig()
		//consoleEncoder := zapcore.NewConsoleEncoder(consoleEncoderConfig)

		// Initialize zap log
		core := zapcore.NewTee(
			zapcore.NewCore(encoder, logWriter, zap.InfoLevel),
			ecszap.NewCore(consoleEncoderConfig, zapcore.AddSync(os.Stdout), zap.InfoLevel),
		)
		//core := zapcore.NewCore(consoleEncoder, w, zap.InfoLevel)
		newLogger := zap.New(core)
		defer newLogger.Sync() // flushes buffer, if any
		globalLogger = newLogger.Sugar()

		return Logger{Logger: globalLogger}
	} else {
		encoderConfig := ecszap.NewDefaultEncoderConfig()
		core := ecszap.NewCore(encoderConfig, os.Stdout, zap.InfoLevel)
		newLogger := zap.New(core, zap.AddCaller())
		defer newLogger.Sync() // flushes buffer, if any
		globalLogger = newLogger.Sugar()
		return Logger{Logger: globalLogger}
	}
}
