package utils

import (
	"math/rand"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type RequestHandler struct {
	Gin *gin.Engine
}

func NewRequestHandler(config Config) RequestHandler {
	gin.SetMode(config.AppMode)
	engine := gin.New()

	engine.Use(func(ctx *gin.Context) {
		ctx.Header("Strict-Transport-Security", "max-age=31536000; includeSubDomains")
		ctx.Header("Cache-Control", "no-store")
		cookie := &http.Cookie{
			Name:     uuid.New().String(),
			Value:    generateRandomString(10),
			Path:     "/",
			HttpOnly: true,
			Secure:   true,
			SameSite: http.SameSiteStrictMode,
		}
		http.SetCookie(ctx.Writer, cookie)
		ctx.Next()
	})

	engine.NoRoute(func(c *gin.Context) {
		c.JSON(http.StatusBadRequest, "Ooops Page Not Found")
	})

	engine.GET("/"+config.Route, func(c *gin.Context) {
		c.JSON(http.StatusOK, "Hello, welcome to Web Service "+config.AppName+"!")
	})

	engine.SetTrustedProxies(nil)
	return RequestHandler{Gin: engine}
}

// Function to generate a random string of a given length
func generateRandomString(length int) string {
	const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	randomString := make([]byte, length)
	for i := range randomString {
		randomString[i] = charset[rand.Intn(len(charset))]
	}
	return string(randomString)
}
