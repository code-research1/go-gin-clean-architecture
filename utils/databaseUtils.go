package utils

import (
	"fmt"

	// "github.com/jinzhu/gorm"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Database struct {
	*gorm.DB
}

func NewDatabase(env Config, logger Logger) Database {
	username := env.DbUsername
	password := env.DbPassword
	host := env.DbUrl
	// host := "postgres"
	port := env.DbPort
	database := env.DbConnection

	url := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", host, port, username, password, database)

	db, err := gorm.Open(postgres.Open(url))

	if err != nil {
		logger.Logger.Panic(err)
	}

	logger.Logger.Info("Database connection established")
	return Database{
		DB: db,
	}
}
