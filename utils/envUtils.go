package utils

import (
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-gin-clean-architecture/constants"
	"github.com/spf13/viper"
)

type Config struct {
	AppName            string
	ServerPort         string
	Environment        string
	AppMode            string
	LogDirectory       string
	DbConnection       string
	DbUsername         string
	DbSchema           string
	DbPassword         string
	DbUrl              string
	DbPort             string
	DbSid              string
	DPass              string
	ApiKey             string
	ApiKeyEncode       string
	SignatureKey       string
	SignatureKeyEncode string
	Route              string
}

func NewEnv() Config {
	var config Config

	viper.SetConfigFile("yaml")
	viper.AddConfigPath("./")
	viper.SetConfigName("config")

	if errData := viper.ReadInConfig(); errData != nil {
		fmt.Printf("Error reading config file, %s", errData)
	}

	config.AppName = viper.GetString("appName")
	config.ServerPort = viper.GetString("serverPort")
	config.Environment = viper.GetString("environment")
	if viper.GetString("releaseMode") == "y" || viper.GetString("releaseMode") == "Y" {
		config.AppMode = gin.ReleaseMode
	} else {
		config.AppMode = gin.DebugMode
	}
	config.LogDirectory = viper.GetString("logDirectory."+config.Environment+".path") + " go-gin-clean-architecture " + time.Now().Format("02-Jan-2006") + ".log"

	dbConnection, _ := DecryptAes256Sha256(viper.GetString("database."+config.Environment+".connection"), constants.KEY_AES)
	config.DbConnection = string(dbConnection)
	dbSchema, _ := DecryptAes256Sha256(viper.GetString("database."+config.Environment+".schema"), constants.KEY_AES)
	config.DbSchema = string(dbSchema)
	dbUsername, _ := DecryptAes256Sha256(viper.GetString("database."+config.Environment+".username"), constants.KEY_AES)
	config.DbUsername = string(dbUsername)
	dbPassword, _ := DecryptAes256Sha256(viper.GetString("database."+config.Environment+".password"), constants.KEY_AES)
	config.DbPassword = string(dbPassword)
	dbUrl, _ := DecryptAes256Sha256(viper.GetString("database."+config.Environment+".url"), constants.KEY_AES)
	config.DbUrl = string(dbUrl)
	dbPort, _ := DecryptAes256Sha256(viper.GetString("database."+config.Environment+".port"), constants.KEY_AES)
	config.DbPort = string(dbPort)

	config.ApiKey = viper.GetString("key." + config.Environment + ".apiKey")
	config.ApiKeyEncode = viper.GetString("key." + config.Environment + ".apiKeyEncode")
	config.SignatureKey = viper.GetString("key." + config.Environment + ".signatureKey")
	config.SignatureKeyEncode = viper.GetString("key." + config.Environment + ".signatureKeyEncode")

	config.Route = viper.GetString("route." + config.Environment + ".name")

	return config
}
