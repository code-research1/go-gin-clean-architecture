package constants

const (
	ERR_TOKEN_EXPIRED   = "Token Expired"
	ERR_TOKEN_MALFORMED = "Token Malformed"
	ERR_TOKEN_HANDLE    = "Couldn't handle token"
	ERR_OOOPSSS         = "Ooopsss errors"

	JWT_VALIDATION_FAILED = "JWT validation failed"

	UNAUTHORIZED_ACCESS    = "Unauthorized access, you do not have authorized access to this function"
	DATA_NOT_FOUND         = "Not found data record"
	DUPLICATE_DATA         = "Data duplication"
	PROVIDED_TOKEN_EXPIRED = "Provided token is expired"
	TOKEN_DECODING         = "An error while decoding token"
	TOKEN_PROVIDED         = "Token not provided"

	ERR_PASSWORD_NOT_MATCH = "Username and password doesn't match."
	ERR_AUTH_CREDENTIAL    = "Authentication credentials were not provided"

	ERR_APP_EXECPTION_TITLE      = "Problem with application"
	ERR_OBJECT_VALIDATION_DETAIL = "Input validation failed"

	ERR_MESSAGE_JSON_FORMAT           = "Json format you entered is wrong"
	ERR_OBJECT_VALIDATION_TITLE       = "Problem with data validation"
	ERR_MESSAGE_BINDING_DATA          = "Please recheck your data binding in the database"
	ERR_MESSAGE_HEADER_API            = "Header Api-Key not match"
	ERR_MESSAGE_HEADER_SIGNATURE      = "Header Signature not match"
	ERR_MESSAGE_HEADER_SIGNATURE_TIME = "Header Signature-Time not match"

	SUCCESSFULLY_ADD    = "Successfully Add"
	SUCCESSFULLY_UPDATE = "Successfully Update"
	SUCCESSFULLY_DELETE = "Successfully Delete"
	SUCCESSFULLY_SEND   = "Successfully Send"
)
