package businessconfig

import (
	"context"

	"github.com/go-gin-clean-architecture/api/controllers"
	"github.com/go-gin-clean-architecture/api/middlewares"
	"github.com/go-gin-clean-architecture/api/routes"
	"github.com/go-gin-clean-architecture/repository"
	"github.com/go-gin-clean-architecture/utils"
	"go.uber.org/fx"
)

var Module = fx.Options(
	controllers.Module,
	routes.Module,
	utils.Module,
	middlewares.Module,
	repository.Module,
	fx.Invoke(businessConfig),
)

func businessConfig(
	lifecycle fx.Lifecycle,
	handler utils.RequestHandler,
	listRoutes routes.Routes,
	env utils.Config,
	logger utils.Logger,
	middlewares middlewares.Middlewares,
	database utils.Database,
) {
	conn, _ := database.DB.DB()

	lifecycle.Append(fx.Hook{
		OnStart: func(context.Context) error {
			logger.Logger.Infof("SERVER STARTED")
			conn.SetMaxOpenConns(10)
			go func() {
				// middlewares.Setup()
				listRoutes.Setup()
				handler.Gin.Run(":" + env.ServerPort)
			}()
			return nil
		},
		OnStop: func(context.Context) error {
			conn.Close()
			logger.Logger.Infof("SERVER STOPPED")
			return nil
		},
	})
}
