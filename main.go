package main

import (
	"github.com/go-gin-clean-architecture/businessconfig"
	"go.uber.org/fx"

	_ "github.com/go-gin-clean-architecture/docs"
)

// @title Go Gin Clean Architecture
// @version 1.0.0
// @description API documentation for GO GIN application
// @termsOfService http://swagger.io/terms/
// @contact.name API Support
// @contact.email  support@swagger.io
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @BasePath /dev/api/v1/go-gin-clean
// @schemes http https
// @securityDefinitions.apikey Bearer
// @in header
// @name Authorization
// @description Provide a Bearer token to authenticate your requests.
func main() {
	fx.New(businessconfig.Module).Run()
}
