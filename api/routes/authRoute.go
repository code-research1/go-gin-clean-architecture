package routes

import (
	"github.com/go-gin-clean-architecture/api/controllers"
	"github.com/go-gin-clean-architecture/utils"
)

// AuthRoutes struct
type AuthRoutes struct {
	logger         utils.Logger
	handler        utils.RequestHandler
	authController controllers.AuthController
	config         utils.Config
}

// Setup user routes
func (s AuthRoutes) Setup() {
	api := s.handler.Gin.Group(s.config.Route)
	{
		api.POST("/login", s.authController.SignIn)
	}
}

// NewAuthRoutes creates new user controller
func NewAuthRoutes(
	handler utils.RequestHandler,
	authController controllers.AuthController,
	logger utils.Logger,
	config utils.Config,
) AuthRoutes {
	return AuthRoutes{
		handler:        handler,
		logger:         logger,
		authController: authController,
		config:         config,
	}
}
