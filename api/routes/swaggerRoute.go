package routes

import (
	"github.com/go-gin-clean-architecture/utils"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

type SwaggerRoutes struct {
	handler utils.RequestHandler
	config  utils.Config
}

func NewSwaggerRoutes(
	handler utils.RequestHandler,
	config utils.Config,
) SwaggerRoutes {
	return SwaggerRoutes{
		handler: handler,
		config:  config,
	}
}

func (r SwaggerRoutes) Setup() {
	api := r.handler.Gin.Group(r.config.Route)
	{
		if r.config.Environment != "prod" {
			api.GET("/docs/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
		}
	}
}
