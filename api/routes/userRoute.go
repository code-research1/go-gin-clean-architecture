package routes

import (
	"github.com/go-gin-clean-architecture/api/controllers"
	"github.com/go-gin-clean-architecture/api/middlewares"
	"github.com/go-gin-clean-architecture/utils"
)

type UserRoutes struct {
	logger         utils.Logger
	handler        utils.RequestHandler
	userController controllers.UserController
	authMiddleware middlewares.AuthMiddleware
	config         utils.Config
}

func (s UserRoutes) Setup() {
	// api := s.handler.Gin.Group("/api").Use(s.authMiddleware.Handler())
	api := s.handler.Gin.Group(s.config.Route).Use(s.authMiddleware.Handler())
	{
		api.GET("/user", s.userController.GetAllDataUser)
		api.POST("/user/getAllDataUserByParam", s.userController.GetAllDataUserByParam)
		api.GET("/user/getDataById/:idMasterUsers", s.userController.GetDataUserById)
		api.POST("/user/store", s.userController.Store)
		api.PUT("/user/update/:idMasterUsers", s.userController.Update)
		api.PUT("/user/updateIsActive/:idMasterUsers", s.userController.UpdateIsActive)
	}
}

func NewUserRoutes(
	logger utils.Logger,
	handler utils.RequestHandler,
	userController controllers.UserController,
	authMiddleware middlewares.AuthMiddleware,
	config utils.Config,
) UserRoutes {
	return UserRoutes{
		handler:        handler,
		logger:         logger,
		userController: userController,
		authMiddleware: authMiddleware,
		config:         config,
	}
}
