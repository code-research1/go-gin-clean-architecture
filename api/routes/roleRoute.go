package routes

import (
	"github.com/go-gin-clean-architecture/api/controllers"
	"github.com/go-gin-clean-architecture/api/middlewares"
	"github.com/go-gin-clean-architecture/utils"
)

type RoleRoutes struct {
	logger         utils.Logger
	handler        utils.RequestHandler
	roleController controllers.RoleController
	authMiddleware middlewares.AuthMiddleware
	config         utils.Config
}

func (s RoleRoutes) Setup() {
	api := s.handler.Gin.Group(s.config.Route).Use(s.authMiddleware.Handler())
	{
		api.GET("/role", s.roleController.GetAllDataRole)
		api.GET("/role/getDataById/:idMasterRoles", s.roleController.GetDataRoleById)
		api.POST("/role/store", s.roleController.Store)
		api.PUT("/role/update/:idMasterRoles", s.roleController.Update)
		api.PUT("/role/updateIsActive/:idMasterRoles", s.roleController.UpdateIsActive)
	}
}

func NewRoleRoutes(
	logger utils.Logger,
	handler utils.RequestHandler,
	roleController controllers.RoleController,
	authMiddleware middlewares.AuthMiddleware,
	config utils.Config,
) RoleRoutes {
	return RoleRoutes{
		handler:        handler,
		logger:         logger,
		roleController: roleController,
		authMiddleware: authMiddleware,
		config:         config,
	}
}
