package routes

import "go.uber.org/fx"

var Module = fx.Options(
	fx.Provide(NewAuthRoutes),
	fx.Provide(NewRoleRoutes),
	fx.Provide(NewUserRoutes),
	fx.Provide(NewCommonRoutes),
	fx.Provide(NewSwaggerRoutes),
	fx.Provide(NewRoutes),
)

// Routes contains multiple routes
type Routes []Route

// Route interface
type Route interface {
	Setup()
}

// NewRoutes sets up routes
func NewRoutes(
	authRoutes AuthRoutes,
	roleRoutes RoleRoutes,
	userRoutes UserRoutes,
	commonRoutes CommonRoutes,
	swaggerRoutes SwaggerRoutes,
) Routes {
	return Routes{
		authRoutes,
		roleRoutes,
		userRoutes,
		commonRoutes,
		swaggerRoutes,
	}
}

// Setup all the route
func (r Routes) Setup() {
	for _, route := range r {
		route.Setup()
	}
}
