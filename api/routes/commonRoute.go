package routes

import (
	"github.com/go-gin-clean-architecture/api/controllers"
	"github.com/go-gin-clean-architecture/utils"
)

type CommonRoutes struct {
	logger           utils.Logger
	handler          utils.RequestHandler
	commonController controllers.CommonController
	config           utils.Config
}

func (s CommonRoutes) Setup() {
	api := s.handler.Gin.Group(s.config.Route)
	{
		api.POST("/encrypt", s.commonController.GetEncryptAES)
		api.POST("/decrypt", s.commonController.GetDecryptAES)
		api.POST("/encryptIsJson", s.commonController.GetEncryptAESIsJson)
		api.POST("/decryptIsJson", s.commonController.GetDecryptAESIsJson)
	}
}

func NewCommonRoutes(
	logger utils.Logger,
	handler utils.RequestHandler,
	commonController controllers.CommonController,
	config utils.Config,
) CommonRoutes {
	return CommonRoutes{
		handler:          handler,
		logger:           logger,
		commonController: commonController,
		config:           config,
	}
}
