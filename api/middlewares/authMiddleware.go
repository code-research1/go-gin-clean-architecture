package middlewares

import (
	"encoding/base64"
	"errors"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-gin-clean-architecture/constants"
	"github.com/go-gin-clean-architecture/model/entity"
	"github.com/go-gin-clean-architecture/model/response"
	"github.com/go-gin-clean-architecture/utils"
	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
)

// JWTAuthMiddleware middleware for jwt authentication
type AuthMiddleware struct {
	env         utils.Config
	logger      utils.Logger
	config      utils.Config
	commonUtils utils.CommonUtils
}

// NewJWTAuthMiddleware creates new jwt auth middleware
func NewAuthMiddleware(
	env utils.Config,
	logger utils.Logger,
	config utils.Config,
	commonUtils utils.CommonUtils,
) AuthMiddleware {
	return AuthMiddleware{
		env:         env,
		logger:      logger,
		config:      config,
		commonUtils: commonUtils,
	}
}

// Setup sets up jwt auth middleware
func (m AuthMiddleware) Setup() {}

// Handler handles middleware functionality
func (m AuthMiddleware) Handler() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result response.ResponseData
		var setHeaderParams response.SetHeaderParams

		authHeader := c.Request.Header.Get("Authorization")

		setHeaderParams.Accept = c.Request.Header.Get("Accept")
		setHeaderParams.ContentType = c.Request.Header.Get("Content-Type")
		setHeaderParams.ApiKey = c.Request.Header.Get("Api-Key")
		setHeaderParams.Signature = c.Request.Header.Get("Signature")
		setHeaderParams.SignatureTime = m.commonUtils.StringToInteger(c.Request.Header.Get("Signature-Time"))

		t := strings.Split(authHeader, " ")
		if len(t) == 2 {
			authToken := t[1]
			setHeaderParams.Authorization = authToken

			authorized, err := m.Authorize(setHeaderParams)
			if authorized {
				c.Next()
				return
			}

			result := m.commonUtils.ResponBody(http.StatusBadRequest,
				constants.ERROR,
				err.Error(),
			)

			go m.commonUtils.CreateLog(
				constants.ERROR,
				constants.ERR_OBJECT_VALIDATION_DETAIL,
				err.Error(),
				http.StatusBadRequest,
				m.commonUtils.GetFunctionName(m.Handler),
				m.commonUtils.GetLineNumber(),
				result,
			)

			c.JSON(http.StatusInternalServerError, result)

			c.Abort()
			return
		}

		result = m.commonUtils.ResponBody(http.StatusUnauthorized,
			constants.ERROR,
			constants.ERR_AUTH_CREDENTIAL,
		)

		c.JSON(http.StatusUnauthorized, result)
		c.Abort()

	}
}

func (m AuthMiddleware) Authorize(setHeaderParams response.SetHeaderParams) (bool, error) {

	signatureKey := []byte(m.env.SignatureKey)
	apiKey := []byte(m.env.ApiKey)

	hash := m.commonUtils.HmacEncode(signatureKey, apiKey)

	if setHeaderParams.ApiKey != m.env.ApiKeyEncode {
		return false, errors.New(strings.ToLower(constants.ERR_MESSAGE_HEADER_API))
	}

	if setHeaderParams.Signature != m.env.SignatureKeyEncode {
		return false, errors.New(strings.ToLower(constants.ERR_MESSAGE_HEADER_SIGNATURE))
	}

	if setHeaderParams.SignatureTime <= 0 {
		return false, errors.New(strings.ToLower(constants.ERR_MESSAGE_HEADER_SIGNATURE_TIME))
	}

	if ok, err := m.commonUtils.HmacDecode(signatureKey, apiKey, hash); !ok {
		if err != nil {
			return false, err
		}
	} else {
		token, err := jwt.Parse(setHeaderParams.Authorization, func(t *jwt.Token) (interface{}, error) {
			return []byte(hash), nil
		})
		if token.Valid {
			return true, nil
		} else if ve, ok := err.(*jwt.ValidationError); ok {
			if ve.Errors&jwt.ValidationErrorMalformed != 0 {
				return false, errors.New(strings.ToLower(constants.ERR_TOKEN_MALFORMED))
			}
			if ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
				return false, errors.New(strings.ToLower(constants.ERR_TOKEN_EXPIRED))
			}
		}
		return false, errors.New(strings.ToLower(constants.ERR_TOKEN_HANDLE))
	}

	return false, errors.New(strings.ToLower(constants.ERR_OOOPSSS))
}

// CreateToken creates jwt auth token
func (m AuthMiddleware) CreateToken(user entity.TableUser) string {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"exp":        time.Now().Add(3 * 24 * time.Hour).Unix(),
		"iat":        time.Now().Unix(),
		"iss":        m.env.ApiKeyEncode,
		"authorized": true,
		"jti":        uuid.New().String(),
	})

	signatureKey, _ := base64.StdEncoding.DecodeString(m.env.SignatureKeyEncode)
	apiKey, _ := base64.StdEncoding.DecodeString(m.env.ApiKeyEncode)

	hash := m.commonUtils.HmacEncode(signatureKey, []byte(apiKey))

	tokenString, err := token.SignedString([]byte(hash))

	if err != nil {

		go m.commonUtils.CreateLog(
			constants.ERROR,
			constants.JWT_VALIDATION_FAILED,
			err.Error(),
			http.StatusBadRequest,
			m.commonUtils.GetFunctionName(m.CreateToken),
			m.commonUtils.GetLineNumber(),
			tokenString,
		)
	}

	return tokenString
}
