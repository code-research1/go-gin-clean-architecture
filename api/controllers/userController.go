package controllers

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/go-gin-clean-architecture/constants"
	"github.com/go-gin-clean-architecture/model/criteria"
	"github.com/go-gin-clean-architecture/model/entity"
	"github.com/go-gin-clean-architecture/model/response"
	"github.com/go-gin-clean-architecture/repository"
	"github.com/go-gin-clean-architecture/utils"
	"github.com/go-gin-clean-architecture/utils/paginate"
	"github.com/go-gin-clean-architecture/validation"
	"github.com/google/uuid"
)

type UserController struct {
	repository  repository.UserRepository
	logger      utils.Logger
	config      utils.Config
	commonUtils utils.CommonUtils
}

func NewUserController(
	repository repository.UserRepository,
	logger utils.Logger,
	config utils.Config,
	commonUtils utils.CommonUtils,
) UserController {
	return UserController{
		repository:  repository,
		logger:      logger,
		config:      config,
		commonUtils: commonUtils,
	}
}

// GetAllDataUser func gets all exists user.
// @Description Get all exists user.
// @Summary get all exists user
// @Tags User
// @Accept application/json
// @Produce application/json
// @Success 200 {object} response.ResponseData
// @Security JWT
// @Router /user [get]
func (s UserController) GetAllDataUser(c *gin.Context) {
	var result response.ResponseData

	paging := paginate.PreparePagination(map[string]string{
		"search":         c.Query("search"),
		"sort_by":        c.Query("sort_by"),
		"sort_direction": c.Query("sort_direction"),
		"limit":          c.Query("limit"),
		"page":           c.Query("page"),
	}, []string{
		"id",
		"created_at",
		"updated_at",
	})

	searchByQuery := c.Query("search_by")
	statusByQUery := c.Query("status")
	status := []string{}
	if statusByQUery != "" {
		status = strings.Split(statusByQUery, ",")
	}

	options := criteria.GetListOfUserOptions{
		SearchBy: searchByQuery,
		Status:   status,
	}

	totalCount, resultData, err := s.repository.GetAllDataUser(c, paging, options)
	if err != nil {

		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			constants.ERR_MESSAGE_JSON_FORMAT,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			err.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.GetAllDataUser),
			s.commonUtils.GetLineNumber(),
			result,
		)

		c.JSON(http.StatusInternalServerError, result)
		return
	}

	data := response.DataPagingResult{
		PageNumber:       paging.Page,
		PageSize:         paging.Limit,
		TotalRecordCount: totalCount,
		Records:          resultData,
	}

	result = s.commonUtils.ResponBody(http.StatusOK,
		constants.SUCCESS,
		data,
	)

	c.JSON(http.StatusOK, result)
}

// GetAllDataUserByParam func gets all exists user by param.
// @Description Get all exists user by param.
// @Summary get all exists user by param
// @Tags User
// @Accept application/json
// @Produce application/json
// @Param request body criteria.UserSearchCriteria true "Request Body"
// @Success 200 {object} response.ResponseData
// @Security JWT
// @Router /user/getAllDataUserByParam [post]
func (s UserController) GetAllDataUserByParam(c *gin.Context) {
	var result response.ResponseData

	var userCriteria criteria.UserSearchCriteria

	errBindJson := c.ShouldBindJSON(&userCriteria)
	if errBindJson != nil {
		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_OBJECT_VALIDATION_TITLE,
			constants.ERR_MESSAGE_JSON_FORMAT,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			errBindJson.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.GetAllDataUserByParam),
			s.commonUtils.GetLineNumber(),
			result,
		)

		c.JSON(http.StatusBadRequest, result)

		return
	}

	users, err := s.repository.GetAllDataUserByParam(c, userCriteria.Key, userCriteria.Value)
	if err != nil {
		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			constants.ERR_MESSAGE_JSON_FORMAT,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			err.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.GetAllDataUserByParam),
			s.commonUtils.GetLineNumber(),
			result,
		)
		c.JSON(http.StatusInternalServerError, result)
		return
	}

	result = s.commonUtils.ResponBody(http.StatusOK,
		constants.SUCCESS,
		users,
	)

	c.JSON(http.StatusOK, result)
}

// GetDataUserById func gets one exists user.
// @Description Get one exists user.
// @Summary get one exists user
// @Tags User
// @Accept application/json
// @Produce application/json
// @Param id path string true "User Id"
// @Success 200 {object} response.ResponseData
// @Security JWT
// @Router /user/getDataById/{idMasterUsers} [get]
func (s UserController) GetDataUserById(c *gin.Context) {
	var result response.ResponseData
	id := c.Param("idMasterUsers")

	datas, err := s.repository.GetDataUserById(c, id)
	if err != nil {

		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			constants.ERR_MESSAGE_JSON_FORMAT,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			err.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.GetDataUserById),
			s.commonUtils.GetLineNumber(),
			result,
		)
		c.JSON(http.StatusInternalServerError, result)
		return
	}

	if datas.IdMasterUsers == "" {
		result := s.commonUtils.ResponBody(http.StatusNotFound,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			constants.DATA_NOT_FOUND,
		)

		c.JSON(http.StatusInternalServerError, result)
		return
	}

	result = s.commonUtils.ResponBody(http.StatusOK,
		constants.SUCCESS,
		datas,
	)

	c.JSON(http.StatusOK, result)
}

// Create func create User.
// @Description create User.
// @Summary create User
// @Tags User
// @Accept application/json
// @Produce application/json
// @Param request body criteria.StoreUserCriteria true "Request Body"
// @Success 200 {object} response.ResponseData
// @Security JWT
// @Router /user/create [post]
func (s UserController) Store(c *gin.Context) {
	var result response.ResponseData
	var objCriteria criteria.StoreUserCriteria

	if err := c.ShouldBind(&objCriteria); err != nil {
		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			constants.ERR_MESSAGE_JSON_FORMAT,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			err.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.Store),
			s.commonUtils.GetLineNumber(),
			result,
		)
		c.JSON(http.StatusInternalServerError, result)
		return
	}

	if validationErr := validation.ValidateCriteria(objCriteria); validationErr != nil {
		result = s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERROR,
			validationErr.Error(),
		)

		c.JSON(http.StatusBadRequest, result)

		return
	}

	genPass, _ := s.commonUtils.GeneratePassword()

	paramStore := entity.TableUser{
		IdMasterUsers: uuid.New().String(),
		IdMasterRoles: objCriteria.IdMasterRoles,
		Fullname:      objCriteria.Fullname,
		Username:      objCriteria.Username,
		IsGender:      objCriteria.IsGender,
		Address:       objCriteria.Address,
		HpNumber:      objCriteria.HpNumber,
		Email:         objCriteria.Email,
		Password:      genPass,
		CreatedBy:     objCriteria.CreatedBy,
		IsActive:      constants.ACTIVED,
	}

	_, err := s.repository.Store(c, paramStore)
	if err != nil {

		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			err,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			err.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.Store),
			s.commonUtils.GetLineNumber(),
			result,
		)

		c.JSON(http.StatusInternalServerError, result)
		return
	}

	result = s.commonUtils.ResponBody(http.StatusOK,
		constants.SUCCESS,
		constants.SUCCESSFULLY_ADD,
	)

	c.JSON(http.StatusOK, result)
}

// Update func update User.
// @Description update User.
// @Summary update User
// @Tags User
// @Accept application/json
// @Produce application/json
// @Param request body criteria.UpdateUserCriteria true "Request Body"
// @Success 200 {object} response.ResponseData
// @Security JWT
// @Router /user/update/{idMasterUsers} [put]
func (s UserController) Update(c *gin.Context) {
	var result response.ResponseData
	var objCriteria criteria.UpdateUserCriteria
	id := c.Param("idMasterUsers")

	if err := c.ShouldBind(&objCriteria); err != nil {
		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			constants.ERR_MESSAGE_JSON_FORMAT,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			err.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.Store),
			s.commonUtils.GetLineNumber(),
			result,
		)
		c.JSON(http.StatusInternalServerError, result)
		return
	}

	if validationErr := validation.ValidateCriteria(objCriteria); validationErr != nil {
		result = s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERROR,
			validationErr.Error(),
		)

		c.JSON(http.StatusBadRequest, result)

		return
	}

	checkData, _ := s.repository.GetDataUserById(c, id)
	if checkData.IdMasterUsers == "" {
		result := s.commonUtils.ResponBody(http.StatusNotFound,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			constants.DATA_NOT_FOUND,
		)

		c.JSON(http.StatusInternalServerError, result)
		return
	}

	paramStore := entity.TableUser{
		IdMasterUsers: id,
		IdMasterRoles: objCriteria.IdMasterRoles,
		Fullname:      objCriteria.Fullname,
		Username:      objCriteria.Username,
		IsGender:      objCriteria.IsGender,
		Address:       objCriteria.Address,
		HpNumber:      objCriteria.HpNumber,
		Email:         objCriteria.Email,
		UpdatedBy:     objCriteria.UpdatedBy,
	}

	_, err := s.repository.Update(c, paramStore)
	if err != nil {

		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			err,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			err.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.Store),
			s.commonUtils.GetLineNumber(),
			result,
		)

		c.JSON(http.StatusInternalServerError, result)
		return
	}

	result = s.commonUtils.ResponBody(http.StatusOK,
		constants.SUCCESS,
		constants.SUCCESSFULLY_UPDATE,
	)

	c.JSON(http.StatusOK, result)
}

// UpdateIsActive func update isActive User.
// @Description update isActive User.
// @Summary update isActive User
// @Tags User
// @Accept application/json
// @Produce application/json
// @Param request body criteria.UpdateIsActiveUserCriteria true "Request Body"
// @Success 200 {object} response.ResponseData
// @Security JWT
// @Router /user/updateIsActive/{idMasterUsers} [put]
func (s UserController) UpdateIsActive(c *gin.Context) {
	var result response.ResponseData
	var objCriteria criteria.UpdateIsActiveUserCriteria
	id := c.Param("idMasterUsers")

	if err := c.ShouldBind(&objCriteria); err != nil {
		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			constants.ERR_MESSAGE_JSON_FORMAT,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			err.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.Store),
			s.commonUtils.GetLineNumber(),
			result,
		)
		c.JSON(http.StatusInternalServerError, result)
		return
	}

	if validationErr := validation.ValidateCriteria(objCriteria); validationErr != nil {
		result = s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERROR,
			validationErr.Error(),
		)

		c.JSON(http.StatusBadRequest, result)

		return
	}

	checkData, _ := s.repository.GetDataUserById(c, id)
	if checkData.IdMasterUsers == "" {
		result := s.commonUtils.ResponBody(http.StatusNotFound,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			constants.DATA_NOT_FOUND,
		)

		c.JSON(http.StatusInternalServerError, result)
		return
	}

	paramStore := entity.TableUser{
		IdMasterUsers: id,
		IsActive:      objCriteria.IsActive,
		UpdatedBy:     objCriteria.UpdatedBy,
	}

	_, err := s.repository.Update(c, paramStore)
	if err != nil {

		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			err,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			err.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.Store),
			s.commonUtils.GetLineNumber(),
			result,
		)

		c.JSON(http.StatusInternalServerError, result)
		return
	}

	result = s.commonUtils.ResponBody(http.StatusOK,
		constants.SUCCESS,
		constants.SUCCESSFULLY_DELETE,
	)

	c.JSON(http.StatusOK, result)
}
