package controllers

import (
	"encoding/json"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-gin-clean-architecture/constants"
	"github.com/go-gin-clean-architecture/model/response"
	"github.com/go-gin-clean-architecture/utils"
)

type CommonController struct {
	logger      utils.Logger
	config      utils.Config
	commonUtils utils.CommonUtils
}

func NewCommonController(
	logger utils.Logger,
	config utils.Config,
	commonUtils utils.CommonUtils,
) CommonController {
	return CommonController{
		logger:      logger,
		config:      config,
		commonUtils: commonUtils,
	}
}

func (s CommonController) GetEncryptAES(c *gin.Context) {
	var result response.ResponseData

	typeKey := c.Request.FormValue("typeKey")
	password := ""

	if typeKey == "DATA" {
		password = constants.KEY_AES
	} else if typeKey == "PASSWORD" {
		password = constants.KEY_PASS_AES
	}

	resultEncrypt, errEncrypt := utils.EncryptAes256Sha256(c.Request.FormValue("param"), password)

	if errEncrypt != nil {

		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			constants.ERR_MESSAGE_JSON_FORMAT,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			errEncrypt.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.GetEncryptAES),
			s.commonUtils.GetLineNumber(),
			result,
		)
		c.JSON(http.StatusInternalServerError, result)
		return
	}

	result = s.commonUtils.ResponBody(http.StatusOK,
		constants.SUCCESS,
		resultEncrypt,
	)

	c.JSON(http.StatusOK, result)
}

func (s CommonController) GetDecryptAES(c *gin.Context) {
	var result response.ResponseData

	typeKey := c.Request.FormValue("typeKey")
	password := ""

	if typeKey == "DATA" {
		password = constants.KEY_AES
	} else if typeKey == "PASSWORD" {
		password = constants.KEY_PASS_AES
	}

	resultDecrypt, errEncrypt := utils.DecryptAes256Sha256(c.Request.FormValue("param"), password)

	if errEncrypt != nil {

		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			constants.ERR_MESSAGE_JSON_FORMAT,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			errEncrypt.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.GetDecryptAES),
			s.commonUtils.GetLineNumber(),
			result,
		)
		c.JSON(http.StatusInternalServerError, result)
		return
	}

	result = s.commonUtils.ResponBody(http.StatusOK,
		constants.SUCCESS,
		resultDecrypt,
	)

	c.JSON(http.StatusOK, result)
}

func (s CommonController) GetEncryptAESIsJson(c *gin.Context) {
	var result response.ResponseData

	var jsonData map[string]interface{}

	if err := c.ShouldBindJSON(&jsonData); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Serialize the JSON data
	jsonDataBytes, err := json.Marshal(jsonData["data"])
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "JSON serialization failed"})
		return
	}

	resultEncrypt, errEncrypt := utils.EncryptAes256Sha256(string(jsonDataBytes), constants.KEY_AES)

	if errEncrypt != nil {

		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			constants.ERR_MESSAGE_JSON_FORMAT,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			errEncrypt.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.GetEncryptAES),
			s.commonUtils.GetLineNumber(),
			result,
		)
		c.JSON(http.StatusInternalServerError, result)
		return
	}

	result = s.commonUtils.ResponBody(http.StatusOK,
		constants.SUCCESS,
		resultEncrypt,
	)

	c.JSON(http.StatusOK, result)
}

func (s CommonController) GetDecryptAESIsJson(c *gin.Context) {
	var result response.ResponseData

	var jsonRequest map[string]interface{}

	if err := c.ShouldBindJSON(&jsonRequest); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Serialize the JSON data
	encryptedData, exists := jsonRequest["encryptedData"]
	if !exists {
		// Handle the case where "encryptedData" is missing
		c.JSON(http.StatusBadRequest, gin.H{"error": "encryptedData is missing"})
		return
	}

	resultDecrypt, errEncrypt := utils.DecryptAes256Sha256(encryptedData.(string), constants.KEY_AES)

	if errEncrypt != nil {

		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			constants.ERR_MESSAGE_JSON_FORMAT,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			errEncrypt.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.GetDecryptAES),
			s.commonUtils.GetLineNumber(),
			result,
		)
		c.JSON(http.StatusInternalServerError, result)
		return
	}

	jsonResponse, _ := s.commonUtils.DetectJSONOrString(resultDecrypt)

	result = s.commonUtils.ResponBody(http.StatusOK,
		constants.SUCCESS,
		jsonResponse,
	)

	c.JSON(http.StatusOK, result)
}
