package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-gin-clean-architecture/api/middlewares"
	"github.com/go-gin-clean-architecture/constants"
	"github.com/go-gin-clean-architecture/model/criteria"
	"github.com/go-gin-clean-architecture/model/response"
	"github.com/go-gin-clean-architecture/repository"
	"github.com/go-gin-clean-architecture/utils"
)

// JWTAuthController struct
type AuthController struct {
	logger         utils.Logger
	authMiddleware middlewares.AuthMiddleware
	config         utils.Config
	commonUtils    utils.CommonUtils
	repository     repository.AuthRepository
}

// NewJWTAuthController creates new controller
func NewAuthController(
	logger utils.Logger,
	authMiddleware middlewares.AuthMiddleware,
	config utils.Config,
	commonUtils utils.CommonUtils,
	repository repository.AuthRepository,
) AuthController {
	return AuthController{
		logger:         logger,
		config:         config,
		commonUtils:    commonUtils,
		authMiddleware: authMiddleware,
		repository:     repository,
	}
}

// Authentication func Authenticate user
// @Description  Takes a create token. Return saved JSON.
// @Summary      authenticate user
// @Tags         Authenticate user
// @Accept       application/json
// @Produce      application/json
// @Param 		 data body criteria.AuthCriteria true  "Request Body"
// @Success      200 {object} response.ResponseData
// @Router       /login [post]
func (s AuthController) SignIn(c *gin.Context) {
	var result response.ResponseData
	var authCriteria criteria.AuthCriteria

	errBindJson := c.ShouldBindJSON(&authCriteria)
	if errBindJson != nil {
		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_OBJECT_VALIDATION_TITLE,
			constants.ERR_MESSAGE_JSON_FORMAT,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			errBindJson.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.SignIn),
			s.commonUtils.GetLineNumber(),
			result,
		)

		c.JSON(http.StatusBadRequest, result)

		return
	}

	passwordDecrypt, errEncrypt := utils.DecryptAes256Sha256(authCriteria.Password, constants.KEY_PASS_AES)

	if errEncrypt != nil {

		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			constants.ERR_PASSWORD_NOT_MATCH,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			errEncrypt.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.SignIn),
			s.commonUtils.GetLineNumber(),
			result,
		)
		c.JSON(http.StatusInternalServerError, result)
		return
	}

	user, err := s.repository.CheckLogin(authCriteria.Username, passwordDecrypt)
	if err != nil {

		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_PASSWORD_NOT_MATCH,
			err,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			err.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.SignIn),
			s.commonUtils.GetLineNumber(),
			result,
		)

		c.JSON(http.StatusInternalServerError, result)
		return
	}

	token := s.authMiddleware.CreateToken(user)

	result = s.commonUtils.ResponBody(http.StatusOK,
		constants.SUCCESS,
		token,
	)

	c.JSON(http.StatusOK, result)
}
