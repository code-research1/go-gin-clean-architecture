package controllers

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/go-gin-clean-architecture/constants"
	"github.com/go-gin-clean-architecture/model/criteria"
	"github.com/go-gin-clean-architecture/model/entity"
	"github.com/go-gin-clean-architecture/model/response"
	"github.com/go-gin-clean-architecture/repository"
	"github.com/go-gin-clean-architecture/utils"
	"github.com/go-gin-clean-architecture/utils/paginate"
	"github.com/go-gin-clean-architecture/validation"
	"github.com/google/uuid"
)

type RoleController struct {
	repository  repository.RoleRepository
	logger      utils.Logger
	config      utils.Config
	commonUtils utils.CommonUtils
}

func NewRoleController(
	repository repository.RoleRepository,
	logger utils.Logger,
	config utils.Config,
	commonUtils utils.CommonUtils,
) RoleController {
	return RoleController{
		repository:  repository,
		logger:      logger,
		config:      config,
		commonUtils: commonUtils,
	}
}

// GetAllDataRole func gets all exists role.
// @Description Get all exists role.
// @Summary get all exists role
// @Tags Role
// @Accept application/json
// @Produce application/json
// @Success 200 {object} response.ResponseData
// @Security JWT
// @Router /role [get]
func (s RoleController) GetAllDataRole(c *gin.Context) {
	var result response.ResponseData

	paging := paginate.PreparePagination(map[string]string{
		"search":         c.Query("search"),
		"sort_by":        c.Query("sort_by"),
		"sort_direction": c.Query("sort_direction"),
		"limit":          c.Query("limit"),
		"page":           c.Query("page"),
	}, []string{
		"id",
		"created_at",
		"updated_at",
	})

	searchByQuery := c.Query("search_by")
	statusByQUery := c.Query("status")
	status := []string{}
	if statusByQUery != "" {
		status = strings.Split(statusByQUery, ",")
	}

	options := criteria.GetListOfRoleOptions{
		SearchBy: searchByQuery,
		Status:   status,
	}

	totalCount, resultData, err := s.repository.GetAllDataRole(c, paging, options)
	if err != nil {

		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			constants.ERR_MESSAGE_JSON_FORMAT,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			err.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.GetAllDataRole),
			s.commonUtils.GetLineNumber(),
			result,
		)
		c.JSON(http.StatusInternalServerError, result)
		return
	}

	data := response.DataPagingResult{
		PageNumber:       paging.Page,
		PageSize:         paging.Limit,
		TotalRecordCount: totalCount,
		Records:          resultData,
	}

	result = s.commonUtils.ResponBody(http.StatusOK,
		constants.SUCCESS,
		data,
	)

	c.JSON(http.StatusOK, result)
}

// GetDataRoleById func gets one exists role.
// @Description Get one exists role.
// @Summary get one exists role
// @Tags Role
// @Accept application/json
// @Produce application/json
// @Param id path string true "Role Id"
// @Success 200 {object} response.ResponseData
// @Security JWT
// @Router /role/getDataById/{idMasterRoles} [get]
func (s RoleController) GetDataRoleById(c *gin.Context) {
	var result response.ResponseData
	id := c.Param("idMasterRoles")

	datas, err := s.repository.GetDataRoleById(c, id)
	if err != nil {

		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			constants.ERR_MESSAGE_JSON_FORMAT,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			err.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.GetDataRoleById),
			s.commonUtils.GetLineNumber(),
			result,
		)
		c.JSON(http.StatusInternalServerError, result)
		return
	}

	if datas.IdMasterRoles == "" {
		result := s.commonUtils.ResponBody(http.StatusNotFound,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			constants.DATA_NOT_FOUND,
		)

		c.JSON(http.StatusInternalServerError, result)
		return
	}

	result = s.commonUtils.ResponBody(http.StatusOK,
		constants.SUCCESS,
		datas,
	)

	c.JSON(http.StatusOK, result)
}

// Create func create Role.
// @Description create Role.
// @Summary create Role
// @Tags Role
// @Accept application/json
// @Produce application/json
// @Param request body criteria.StoreRoleCriteria true "Request Body"
// @Success 200 {object} response.ResponseData
// @Security JWT
// @Router /role/create [post]
func (s RoleController) Store(c *gin.Context) {
	var result response.ResponseData
	var objCriteria criteria.StoreRoleCriteria

	if err := c.ShouldBind(&objCriteria); err != nil {
		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			constants.ERR_MESSAGE_JSON_FORMAT,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			err.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.Store),
			s.commonUtils.GetLineNumber(),
			result,
		)
		c.JSON(http.StatusInternalServerError, result)
		return
	}

	if validationErr := validation.ValidateCriteria(objCriteria); validationErr != nil {
		result = s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERROR,
			validationErr.Error(),
		)

		c.JSON(http.StatusBadRequest, result)

		return
	}

	paramStore := entity.TableRoles{
		IdMasterRoles: uuid.New().String(),
		RoleName:      objCriteria.RoleName,
		Description:   objCriteria.Description,
		CreatedBy:     objCriteria.CreatedBy,
		IsActive:      constants.ACTIVED,
	}

	_, err := s.repository.Store(c, paramStore)
	if err != nil {

		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			err,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			err.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.Store),
			s.commonUtils.GetLineNumber(),
			result,
		)

		c.JSON(http.StatusInternalServerError, result)
		return
	}

	result = s.commonUtils.ResponBody(http.StatusOK,
		constants.SUCCESS,
		constants.SUCCESSFULLY_ADD,
	)

	c.JSON(http.StatusOK, result)
}

// Update func update Role.
// @Description update Role.
// @Summary update Role
// @Tags Role
// @Accept application/json
// @Produce application/json
// @Param request body criteria.UpdateRoleCriteria true "Request Body"
// @Success 200 {object} response.ResponseData
// @Security JWT
// @Router /role/update/{idMasterRoles} [put]
func (s RoleController) Update(c *gin.Context) {
	var result response.ResponseData
	var objCriteria criteria.UpdateRoleCriteria
	id := c.Param("idMasterRoles")

	if err := c.ShouldBind(&objCriteria); err != nil {
		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			constants.ERR_MESSAGE_JSON_FORMAT,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			err.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.Store),
			s.commonUtils.GetLineNumber(),
			result,
		)
		c.JSON(http.StatusInternalServerError, result)
		return
	}

	if validationErr := validation.ValidateCriteria(objCriteria); validationErr != nil {
		result = s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERROR,
			validationErr.Error(),
		)

		c.JSON(http.StatusBadRequest, result)

		return
	}

	checkData, _ := s.repository.GetDataRoleById(c, id)
	if checkData.IdMasterRoles == "" {
		result := s.commonUtils.ResponBody(http.StatusNotFound,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			constants.DATA_NOT_FOUND,
		)

		c.JSON(http.StatusInternalServerError, result)
		return
	}

	paramStore := entity.TableRoles{
		IdMasterRoles: id,
		RoleName:      objCriteria.RoleName,
		Description:   objCriteria.Description,
		UpdatedBy:     objCriteria.UpdatedBy,
	}

	_, err := s.repository.Update(c, paramStore)
	if err != nil {

		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			err,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			err.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.Store),
			s.commonUtils.GetLineNumber(),
			result,
		)

		c.JSON(http.StatusInternalServerError, result)
		return
	}

	result = s.commonUtils.ResponBody(http.StatusOK,
		constants.SUCCESS,
		constants.SUCCESSFULLY_UPDATE,
	)

	c.JSON(http.StatusOK, result)
}

// UpdateIsActive func update isActive Role.
// @Description update isActive Role.
// @Summary update isActive Role
// @Tags Role
// @Accept application/json
// @Produce application/json
// @Param request body criteria.UpdateIsActiveRoleCriteria true "Request Body"
// @Success 200 {object} response.ResponseData
// @Security JWT
// @Router /role/updateIsActive/{idMasterRoles} [put]
func (s RoleController) UpdateIsActive(c *gin.Context) {
	var result response.ResponseData
	var objCriteria criteria.UpdateIsActiveRoleCriteria
	id := c.Param("idMasterRoles")

	if err := c.ShouldBind(&objCriteria); err != nil {
		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			constants.ERR_MESSAGE_JSON_FORMAT,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			err.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.Store),
			s.commonUtils.GetLineNumber(),
			result,
		)
		c.JSON(http.StatusInternalServerError, result)
		return
	}

	if validationErr := validation.ValidateCriteria(objCriteria); validationErr != nil {
		result = s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERROR,
			validationErr.Error(),
		)

		c.JSON(http.StatusBadRequest, result)

		return
	}

	checkData, _ := s.repository.GetDataRoleById(c, id)
	if checkData.IdMasterRoles == "" {
		result := s.commonUtils.ResponBody(http.StatusNotFound,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			constants.DATA_NOT_FOUND,
		)

		c.JSON(http.StatusInternalServerError, result)
		return
	}

	paramStore := entity.TableRoles{
		IdMasterRoles: id,
		IsActive:      objCriteria.IsActive,
		UpdatedBy:     objCriteria.UpdatedBy,
	}

	_, err := s.repository.Update(c, paramStore)
	if err != nil {

		result := s.commonUtils.ResponBody(http.StatusBadRequest,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			err,
		)

		go s.commonUtils.CreateLog(
			constants.ERROR,
			constants.ERR_OBJECT_VALIDATION_DETAIL,
			err.Error(),
			http.StatusBadRequest,
			s.commonUtils.GetFunctionName(s.Store),
			s.commonUtils.GetLineNumber(),
			result,
		)

		c.JSON(http.StatusInternalServerError, result)
		return
	}

	result = s.commonUtils.ResponBody(http.StatusOK,
		constants.SUCCESS,
		constants.SUCCESSFULLY_DELETE,
	)

	c.JSON(http.StatusOK, result)
}
